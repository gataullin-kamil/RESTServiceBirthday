package com.evelopers.rest;

import com.evelopers.rest.domain.Note;
import org.apache.log4j.Logger;
import org.springframework.core.io.ClassPathResource;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.Callable;

import static java.util.Calendar.*;

/**
 * @author Kamil
 *         01.10.2016
 */
public class Task implements Callable<List<Note>> {

    private static final Logger log = Logger.getLogger(Task.class);

    private static final String FILE_PATH = "birthdays.csv";
    private static final String DATE_PATTERN = "dd.MM.yyyy";

    private Integer month;

    public Task(Integer month) {
        this.month = month;
    }

    @Override
    public List<Note> call() throws Exception {
        List<Note> notes = getNotes();
        try {
            Thread.sleep(20 * 1000);
        } catch (InterruptedException e) {
            log.error("Error in sleep: " + e.getMessage());
        }
        return notes;
    }

    /**
     * Получение списка людей родившихся в заданный месяц
     */
    private List<Note> getNotes() {
        final List<Note> answers = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(
                new ClassPathResource(FILE_PATH).getInputStream(), StandardCharsets.UTF_8))) {
            String line;
            while ((line = reader.readLine()) != null) {
                Note note = getNote(line);
                if (note != null) {
                    answers.add(note);
                }
            }
        } catch (IOException e) {
            log.error("Error in read from file: " + e.getMessage());
        }
        return answers;
    }

    /**
     * @param line строка из файла
     * @return запись удовлетворяющая заданному месяцу или null
     */
    private Note getNote(String line) {
        String[] mas = line.split(";");
        if (mas.length != 2) {
            log.error("Incorrect file format. Line = '" + line + "'");
            return null;
        }
        Calendar birthday = getCalendar(mas[1]);
        if (birthday != null && month == birthday.get(MONTH) + 1) {
            return new Note(mas[0], getDiffForBirthday(birthday));
        }
        return null;
    }

    private Calendar getCalendar(String dateStr) {
        try {
            Calendar birthday = new GregorianCalendar();
            birthday.setTime(new SimpleDateFormat(DATE_PATTERN).parse(dateStr));
            return birthday;
        } catch (ParseException e) {
            log.error("Can not parse date: " + dateStr + ". Date should meet pattern " + DATE_PATTERN);
            return null;
        }
    }

    /**
     * Вычисляем кол-во дней до ближайшего дня рождения
     * @param birthday день рождения
     */
    private Integer getDiffForBirthday(Calendar birthday) {
        Integer diff;
        Calendar today = new GregorianCalendar();
        Calendar birthdayInCurrentYear = new GregorianCalendar(
                today.get(YEAR),
                birthday.get(MONTH),
                birthday.get(DAY_OF_MONTH));
        if (birthdayInCurrentYear.after(today)) {
            // если дня рождения в этом году еще не было
            diff = birthdayInCurrentYear.get(DAY_OF_YEAR) - today.get(DAY_OF_YEAR);
        } else {
            // если в этом году день рождения уже был
            birthdayInCurrentYear.set(YEAR, today.get(YEAR) + 1);
            int dayInYear = new GregorianCalendar().isLeapYear(today.get(YEAR)) ? 366 : 365;
            diff = birthdayInCurrentYear.get(DAY_OF_YEAR) + dayInYear - today.get(DAY_OF_YEAR);
        }
        return diff;
    }
}
