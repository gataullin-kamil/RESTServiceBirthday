package com.evelopers.rest.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Kamil
 *         01.10.2016
 */
public class Result {

    /**
     * Список записей
     */
    private List<Note> people;

    /**
     * Код ошибки:
     * 0 - успех
     * 1 - неверное id процесса
     * 2 - процесс еще не завершен
     * 3 - другая ошибка на сервере
     */
    private Integer errorCode;

    /**
     * Текст ошибки
     */
    private String errorMessage;

    public Result(List<Note> people) {
        this.people = people;
        this.errorCode = 0;
        this.errorMessage = "";
    }

    public Result(Integer errorCode, String errorMessage) {
        this.people = new ArrayList<>();
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public List<Note> getPeople() {
        return people;
    }

    public void setPeople(List<Note> people) {
        this.people = people;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
