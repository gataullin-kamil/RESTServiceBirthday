package com.evelopers.rest.domain;

/**
 * @author Kamil
 *         01.10.2016
 */
public class ProcessID {

    /**
     * ID процесса
     */
    private String id;

    public ProcessID(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
