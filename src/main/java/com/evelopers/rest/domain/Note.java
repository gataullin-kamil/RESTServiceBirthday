package com.evelopers.rest.domain;

/**
 * @author Kamil
 *         01.10.2016
 */
public class Note {

    /**
     * ФИО
     */
    private String name;

    /**
     * Количество дней до дня рождения
     */
    private Integer daysCount;

    public Note(String name, Integer daysCount) {
        this.name = name;
        this.daysCount = daysCount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getDaysCount() {
        return daysCount;
    }

    public void setDaysCount(Integer daysCount) {
        this.daysCount = daysCount;
    }

    @Override
    public String toString() {
        return "Note{" +
                "name='" + name + '\'' +
                ", daysCount=" + daysCount +
                '}';
    }
}
