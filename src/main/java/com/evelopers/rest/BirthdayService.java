package com.evelopers.rest;

import com.evelopers.rest.domain.Note;
import com.evelopers.rest.domain.ProcessID;
import com.evelopers.rest.domain.Result;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * @author Kamil
 *         01.10.2016
 */
@Service
public class BirthdayService {

    private static final Logger log = Logger.getLogger(BirthdayService.class);

    private Map<String, Future> processes = new HashMap<>();

    /**
     * Запуск вычислений
     *
     * @param month месяц
     * @return ID процесса
     */
    public ProcessID start(final Integer month) {
        ExecutorService executor = Executors.newCachedThreadPool();
        Future<List<Note>> process = executor.submit(new Task(month));
        String processId = String.valueOf(new Date().getTime());
        processes.put(processId, process);
        log.info("Start process with id = " + processId + " for month = " + month);
        return new ProcessID(processId);
    }

    /**
     * Получение результата вычислений
     *
     * @param processId ID процесса
     * @return результат вычислений, либо сообщение об ошибке
     */
    public Result getResult(String processId) {
        if (!processes.containsKey(processId)) {
            log.warn("No process with id = " + processId);
            return new Result(1, "No process with id = " + processId);
        } else if (!processes.get(processId).isDone()) {
            log.info("The process with id = " + processId + " is not yet complete");
            return new Result(2, "The process is not yet complete");
        } else {
            try {
                Result result = new Result((List<Note>) processes.get(processId).get());
                processes.remove(processId);
                log.info("Returned result for process with id = " + processId);
                return result;
            } catch (InterruptedException | ExecutionException e) {
                log.error("Error in server: " + e.getMessage());
                return new Result(3, "Error in server");
            }
        }
    }

}
