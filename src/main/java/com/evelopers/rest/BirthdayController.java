package com.evelopers.rest;

import com.evelopers.rest.domain.ProcessID;
import com.evelopers.rest.domain.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * @author Kamil
 *         01.10.2016
 */
@RestController
public class BirthdayController {

    private static final String RM_START = "/start";
    private static final String RM_RESULT = "/result";

    @Autowired
    private BirthdayService birthdayService;

    @RequestMapping("/")
    public String main() {
        return "";
    }

    @RequestMapping(value = RM_START, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ProcessID startProcess(@RequestParam(required = false) Integer month) {
        if (month == null) {
            month = new GregorianCalendar().get(Calendar.MONTH) + 1;
        }
        return birthdayService.start(month);
    }

    @RequestMapping(value = RM_RESULT, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Result getResult(@RequestParam String id) {
        return birthdayService.getResult(id);
    }
}
